from typing import Union

import asyncpg
from asyncpg import Connection
from asyncpg.pool import Pool
from .settings import DB_USER, DB_HOST, DB_NAME, DB_PASSWORD


class Database:
    def __init__(self):
        self.pool: Union[Pool, None] = None

    async def create(self):
        self.pool = await asyncpg.create_pool(
            user=DB_USER,
            password=DB_PASSWORD,
            host=DB_HOST,
            database=DB_NAME
        )

    async def execute(self, command, *args, fetch: bool = False, fetchval: bool = False, fetchrow: bool = False,
                      execute: bool = False, executemany: bool = False):
        async with self.pool.acquire() as connection:
            connection: Connection
            async with connection.transaction():
                if fetch:
                    result = await connection.fetch(command, *args)
                elif fetchval:
                    result = await connection.fetchval(command, *args)
                elif fetchrow:
                    result = await connection.fetchrow(command, *args)
                elif execute:
                    result = await connection.execute(command, *args)
                elif executemany:
                    result = await connection.executemany(command, *args)
            return result

    async def save_tg_user(self, tg_id, username, fullname):
        """ сохранение информации о юзере """
        sql = """INSERT INTO app_tguser(tg_id, username, fullname) VALUES ($1, $2, $3) ON CONFLICT DO NOTHING"""
        await self.execute(sql, tg_id, username, fullname, fetchrow=True)

    async def update_phone(self, tg_id, phone):
        """ обновление номера телефона у юзера """
        sql = "UPDATE app_tguser SET phone_number=$1 WHERE tg_id=$2"
        return await self.execute(sql, phone, tg_id, fetchrow=True)

    async def check_phone(self, tg_id):
        """ если у пользователя в бд записан номер, то вернется вся информация о нем, иначе None"""
        sql = "SELECT * FROM app_tguser WHERE tg_id=$1"
        result = await self.execute(sql, tg_id, fetchrow=True)

        return result if result and result['phone_number'] is not None \
            else None


db = Database()
