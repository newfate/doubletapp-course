from rest_framework import serializers

from app.internal.models.tg_user import TgUser


class TgUserSerializer(serializers.Serializer):
    tg_id = serializers.IntegerField()
    phone_number = serializers.CharField(max_length=20)
    username = serializers.CharField(max_length=255)
    fullname = serializers.CharField(max_length=255)
