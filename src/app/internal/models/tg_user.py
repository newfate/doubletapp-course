from django.db import models


class TgUser(models.Model):
    tg_id = models.BigIntegerField(unique=True, verbose_name='id телеграма')
    phone_number = models.CharField(max_length=20, blank=True, null=True, verbose_name='Телефон')
    username = models.CharField(max_length=255, blank=True, null=True, verbose_name='Логин')
    fullname = models.CharField(max_length=255, verbose_name='ФИ')

    class Meta:
        verbose_name = 'Тг пользователь'
        verbose_name_plural = 'Тг пользователи'

    def __str__(self):
        return str(self.id)
