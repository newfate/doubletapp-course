import sys
from pathlib import Path

from aiogram import Bot, Dispatcher, executor, types

path = Path(__file__).parents[2]
sys.path.append(str(path))


async def main(dispatcher):
    from transport.bot.handlers import send_welcome, send_phone_button, save_contact, get_me
    from config.async_database import db

    await db.create()

    dispatcher.register_message_handler(send_welcome, commands='start')
    dispatcher.register_message_handler(send_phone_button, commands='set_phone')
    dispatcher.register_message_handler(save_contact, content_types=types.ContentType.CONTACT)
    dispatcher.register_message_handler(get_me, commands='me')


if __name__ == '__main__':
    """ Запуск бота. Если нужно пропускать обработку пропущенных событий(при отключенном боте), 
    то после аргумента on_startup - добавить skip_updates=True"""
    from config.settings import BOT_TOKEN

    bot = Bot(token=BOT_TOKEN)
    dp = Dispatcher(bot)

    executor.start_polling(dp, on_startup=main)
