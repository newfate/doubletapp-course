from django.contrib import admin

from ..models.tg_user import TgUser


@admin.register(TgUser)
class TgUserAdmin(admin.ModelAdmin):
    pass
    list_display = ['id', 'username', 'fullname', 'phone_number', 'tg_id']
    search_fields = ['username', 'td_id', 'fullname']
    readonly_fields = ['username', 'tg_id']
