from django.urls import path

from app.internal.transport.rest.handlers import TgUserAPIView

urlpatterns = [
    path("me", TgUserAPIView.as_view())
]
