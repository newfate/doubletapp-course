from aiogram import types

from app.internal.services.bot_services import send_phone_button, update_user_info, save_user, get_user_info


async def send_welcome(message: types.Message):
    """ Сохраняет в БД юзера """
    await save_user(message)


async def set_phone(message: types.Message):
    """ Добавляет кнопку в меню, для отправки номера телефона, привязанного к телеграм """
    await send_phone_button(message)


async def save_contact(message: types.Message):
    """ сохраняет номер телефона, логин(если есть) и ФИ  в БД """
    await update_user_info(message)


async def get_me(message: types.Message):
    """ отправляем пользователю его данные, которые хранятся в БД """
    await get_user_info(message)
