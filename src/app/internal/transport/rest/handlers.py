from django.http import JsonResponse
from django.views import View
from rest_framework import status

from app.models import TgUser
from app.serializers import TgUserSerializer


class TgUserAPIView(View):
    def get(self, request):
        tg_id = request.GET.get('tg_id')

        # если не ввели параметр tg_id
        if tg_id is None:
            return JsonResponse({'status': 'error', 'info': "parametr 'tg_id' is empty"},
                                status=status.HTTP_400_BAD_REQUEST)

        user = TgUser.objects.filter(tg_id=tg_id).first()

        # пользователь не найден
        if user is None:
            return JsonResponse({'status': 'error', 'info': "User not Found"}, status=status.HTTP_404_NOT_FOUND)

        serialized_user = TgUserSerializer(user).data
        phone = serialized_user['phone_number']

        # телефон не заполнен
        if phone is None:
            return JsonResponse({'status': 'error', 'info': "you don't have access! Please send your number via "
                                                            "the 'set_phone' command of the bot ***BOT"},
                                status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        return JsonResponse({'status': 'successfully', 'info': serialized_user}, status=status.HTTP_200_OK)
