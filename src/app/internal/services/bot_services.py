from aiogram import types
from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardRemove

from config.async_database import db


async def save_user(message: types.Message):
    tg_id = message.from_user.id
    username = message.from_user.username if message.from_user.username else ''
    fullname = message.from_user.full_name
    await db.save_tg_user(tg_id, username, fullname)
    await message.reply("Приветствую! Чтобы получить доступ к функционалу бота - отправь команду /set_phone")


async def send_phone_button(message: types.Message):
    phone_keyboard = ReplyKeyboardMarkup(row_width=1)
    phone_keyboard.add(KeyboardButton(text='Отправить номер', request_contact=True))

    await message.answer('Нажмите на кнопку в меню, чтобы отправить свой телефон⬇️', reply_markup=phone_keyboard)


async def update_user_info(message: types.Message):
    phone_number = message.contact.phone_number
    await db.update_phone(message.from_user.id, phone_number)
    await message.answer("Ваши данные обновлены! Чтобы посмотреть информацию о себе - отправьте /me",
                         reply_markup=ReplyKeyboardRemove())


async def get_user_info(message: types.Message):
    check = await db.check_phone(message.from_user.id)
    if check:
        text = f"Ваш id: {check['tg_id']}" \
               f"\nФИ: {check['fullname']}" \
               f"\nЛогин: {check['username']}" \
               f"\nТелефон: {check['phone_number']}"
    else:
        text = "Чтобы получить доступ к этой функции - отправьте свой номер через /set_phone"

    await message.answer(text)
