# Скачивание проекта
`git clone https://gitlab.com/newfate200135/doubletapp-course.git` - клонирование репозитория

`python -m venv venv` - создание виртуального окружения

`venv\Scripts\activate` - активировать вирт. окружение на Windows

`source venv\bin\activate` - активировать вирт. окружение на Linux

`pip install -r requirements.txt` - установка зависимостей

# Конфиг
В папке src\config лежит файл .env.example - изменить на .env и внести свои данные от Postgres и токен бота

# Запуск Django
`cd src`

Если установлен make:

`make migrate`

`make createsuperuser` - заполняете данные

`make dev` 

Если make отсутствует:

`python manage.py migrate`

`python manage.py createsuperuser` - заполняете данные

`python manage.py runserver localhost:8000`

Затем перейти по адресу http://localhost:8000/admin/

# Запуск тг-бота

`python src\app\internal\bot.py`


# API запрос
Доступен один get-эндпоинт, с параметром tg_id - http://localhost:8000/api/me?tg_id=XXX